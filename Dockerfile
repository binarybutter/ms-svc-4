FROM python:3.6-alpine3.6
COPY src /app
WORKDIR /app
RUN pip install -r requirements.txt
EXPOSE 8080
ENTRYPOINT ["python","-u","/app/cluster_properties.py"]
